I would like to note...

IN GENERAL:

SCENE 1: Ang Pamilya ni Bida
        Medyo okaay naman yung part na 'to para sa 'kin. Pero napansin ko lang habang binabasa ko
    siya, parang may pagka-"generic" yung paraan ng pagpakita na mahirap lang sila. Tingin ko may
    mas magandang pang paraan para maipakita na hindi sila "may kaya". Yung hindi gumagamit ng
    pagke-kwento _lang_ tungkol sa mga pangyayari sa buhay nila. Kailangan dito yung mas may impact
    sana sa mga manonood. Kasi kailangan yun lalo na't unang-unang scene siya.
        Isa rin pala, gets ko na pinapakita dito sa scene na 'to na mahirap lang ang pamilya nila.
    Kaso parang masyado naman yatang na-focus yung dialogue dun sa "mahirap lang yung pamilya nila",
    imbis na "mahirap lang yung bida, pati na ang pamilya niya". Kumbaga, dapat naka-sentro
    pa rin yung storytelling doon sa bida. Kasi kwento niya yun, at hindi nung mga kapatid niya o mga
    magulang niya. Syempre, 'di naman maiiwasan na nandoon rin sa eksena yung mga karakter na 'yon pero
    dapat sa kahit anong sitwasyon na kasama sila, may active role pa rin yung bida. Hindi yung parang
    nasa-sideline siya, tuwing magke-kwento yung iba. Pero dito, opinion ko nalang naman yan.

SCENE 2: Munting Sorpresa
        Parang may mga nakita akong incosistency sa scene na 'to. Una, panong nakatanggap yung nanay
    ng tawag mula dun sa eskwelahan? May telepono ba sila sa bahay, may cellphone? Kung meron nga,
    parang 'di naman ata pang-mahirap ang tunog nun. Sunod, paano namang nakuha ng eskwelahan yung
    telepono nila? At bakit sa telepono ipinaalam? Hindi ba dapat personal yun sasabihin dun sa bata
    o kaya magulang? Pero pinakamahalaga sa lahat, bakit napili yung bida na mabigyan ng scholarship?
    Masipag ba siyang mag-aaral? Nanalo ba siya sa kumpetisyon? Nag-apply ba sila?
        Sa tingin ko parang masyadong naging convenient yung scenario na nangyari dito. Naging halata
    na plot device lang yung pagkakataon na nabigay sa kanya, imbis na isang bagay na hinanap talaga
    niya. Kailangan dito ng scenario na may pulidong rason kung bakit at paano siya nabigyan ng
    pagkakataon na makakuha yung scholarship.

SCENE 3: Scholarship Application
        Sobrang dami nangyari na scene na 'to pero ang napansin ko lang, parang masyado naman yatang
    madali ang naging pagkuha niya ng scholarship. Para sa 'kin kasi, kung madali ang pagkuha niya
    ng scholarship dapat mabilis o kaunti lang ang mga pangyayari na magpapakita noon. Kung mahirap
    naman o matagal, dapat rin matagal ang pagpapakita kung paano niya nakuha yung bagay na inaasam
    niya. Ang nangyari kasi madali lang niyang nakuha yung inaasam niyang scholarship, pero yung
    proseso na pinakita ang haba. Kumbaga, kung sa huli, makukuha rin lang pala niya yung scholarship,
    ng ganon-ganon lang, anong silbi na pinahaba pa yung paglalahad ng kwento.
        Ilan lang sa mani-nitpick ko yung sa simula. Tingin ko hindi naman masama na pinakita yung
    pag-alis niya papunta sa kung saan man. Pero ang dating kasi sa 'kin, ang kinahantungan lang ng
    part na yun, pinakita lang ulit ang mga bagay na alam, o obvious na, sa audience, e.g. mahirap
    sila, mabait siyang anak. Kung ilalagay yung parte na yun, dapat may mas makabuluhan siyang
    layunin sa pag-develop ng kwento.
        Sa mga sumunod pang parte ng scene, ganon rin masasabi ko. Hindi naman entirely pointless
    na nandoon sila. Pero tingin ko kung ilalagay sila, may mga mas importanteng mangyayari dapat.
    Kung hindi sila mabibigyan ng ganong role, mas mainam sigurong nakasingit nalang sila, imbis na
    may dialogue pa.

SCENE 4: Hirap sa Pagluluwas
        Base sa pagkakaintindi ko, pinapakita lang sa scene na 'to na ultimo panluwas nalang ng
    Maynila hindi pa rin sila makahanap ng pera. Tingin ko isa rin 'to sa mga scenes na pwedeng pang
    bigyan ng mas malalim na kabuluhan. Pinakita lang kasi dito na dahil wala silang perang panluwas,
    mahuhuli siya sa pagpasok. Ngunit, ano naman ang magiging epekto sa storya kung huli siyang
    pumasok? Ipinakita sa eksena na maaaring magalit o madismaya yung Dean na nakausap nila. Kung
    eto ang magiging epekto sa storya ng nahuling pagluwas niya, ano naman ang gagawin ng Dean kung
    madismaya siya? Tingin ko pwede pang mas mabigyan ng bigat yung problema na pinakita dito. Hindi
    lang yung tipong may maiinis o magagalit.
        Idaragdag ko na rin, dahil parte siya ng eksena, na masyado ring naging madali ang pagresolba
    ng problema nila. Nagmukha ulit siyang masyadong "convenient". Pinag-usapan lang nila na "baka
    magalit" yung isang karakter, bigla ng may tumawag para isiguro sa kanila na mabuti lang ang
    lahat, kahit anong mangyari. Sa ganong punto, parang naglaho na nga talaga yung bigat nung
    pinakilalang suliranin dito. Naging sitcom kuno yung eksena dito, except walang comedy.

SCENE 5: Paglisan ni Bida
        Okaay naman yung scene na 'to overall. Pero tingin ko mas pwede pang habaan yung heart-to-heart
    moment nilang pamilya. Dapat maramdaman dito ng mga manonood na malungkot yung nangyayari dahil
    aalis na yung bida. Meron ding dapat na konting takot dahil hindi niya alam kung anong mga
    mararanasan niya sa Maynila. Ngunit, may halo ring dapat na saya kasi unti-unti na siyang
    lumalapit sa pagtupad ng mga pangarap niya. Alam kong mahirap gawin yung ganyan kaya no pressure (:
        >inb4 "di naman kita minamadale, pero as soon as possible sana"

SCENE 6: Bagong Kaibigan ni Bida
        Medyo okay na rin yung scene na 'to. Masa-suggest ko nalang na sana yung pag-uusap nila ng
    roommate niya mas mahaba pa ng kaunti. Nag-usap kasi sila pero ang nalaman ko lang magkaparehas
    sila ng university. Ano ba personality nung roommate niya? Anong pangalan niya? Bakit niya
    naisip na mag-aral din sa unibersidad napili niya? Bakit hindi niya natanong mga bagay na 'yon
    sa bida? Yun lang naman, kasi parang conjunction lang naman scene na 'to. Pero tulad ng sabi ko,
    bawat eksena may layunin dapat.

SCENE 7: Engkwentro with the Bullies
        Good na yung scene na 'to. Pero kakaiba lang talaga na sa Dean pa kukuha ng schedule ng klase.

SCENE 8: Heart-to-Heart ni Bida at Roommate
        Dito nagkaron yung bida at yung roommate niya ng parang "emotional" na moment na tugma naman
    naman sa mga nangyari dun sa huling eksena. Kaunting problem lang na nakita ko, bakit parang
    ang protective na kagad nung roommate dun sa bida, kahit na hindi pa naman sila ganon ka-close,
    gaya nga ng pinakita sa mga naunang scene.
        Sa tingin ko dapat ipakita muna dun sa mga naunang scene na may some form of friendship sila,
    bago lalabas tong parte eksena na to na pinoprotektahan nila ang isa't isa. Kumbaga, okay naman
    yung mga nangyari dito sa scene na 'to pero dapat masuportahan muna siya ng kaunti pa ng mga
    naunang scenes.

SCENE 9: Pang-araw-araw na Pamumuhay ni Bida
        Karamihan ng mga pinakita dito, mga routine niya lang sa university nila. Magtatrabaho,
    papasok, uuwi, at maghahanap ng trabaho. For the most part, okay naman na yung mga eksena dito
    so ang maisa-suggest ko nalang yung sa part ng cafeteria. Nabanggit kasi sa unang bahagi ng scene
    na to na yung mga dating naging helper dun sa cafeteria nila, mga scholar din. Sila raw ngayon may
    mga trabaho na, kaya dapat magsumikap rin daw si bida para maging tulad nila.
        Tingin ko, tutal nabanggit na rin sila. Pwede mo na rin idagdag dito kung ano yung background
    ng mga nauna. Ano naging mga trabaho nila? Masipag rin ba sila? Eh yung tindera ba, gano katagal
    na siyang tatrabaho dun sa cafeteria? Kahit minor details lang tungkol sa kanila para hindi kakaiba
    yung pagtapos nung part na yun.
        Isa rin pala, nakakapagtaka yung kabaitan nung tindera. Tingin ko, kung ganon ang gustong
    nating ipakita na personality niya, dapat pati siya may konting background. Kahit yung tipong
    magkekwento lang siya ng isang bagay tungkol sa sarili niya, katulad nung kay Roommate.
        Dun sa parteng tumawag yung bida sa nanay niya, tingin ko pwede pang mas mabigyang kabuluhan
    yung pagtawag niya. Pwede kakamustahin siya ng nanay niya tungkol sa mga kasama niya. Kamusta 
    yung pag-aaral niya, at kung nakakatulog pa siya ng maayos. Yung simpleng ganon, pwedeng maipakita
    yung bond nila ng nanay niya. Maisisingit na rin din yung mga kapatid niya at tatay niya. Dapat
    simboliko sana yung bawat eksena na tulad nito.
        Yung ibang scenes masasabi ko rin medyo nangangailangan ng ganon, tulad nung mga interaksyon
    niya kay roommate pati na sa mga bullies.

SCENE 10:
        Napansin ko sa scene na to na parang masyadong naging unprofessional yung manager nila.
    Sinisigaw-sigawan niya pa yung bida at kinukutya. Kung magdaragdag ng mga ganong parts, mainam
    siguro na ipakita na rin dito yung pagdi-diskrimina sa kanya. So far kasi walang nangyayaring
    ganon. Ang lumalabas lang 1) Mahirap sila 2) Binu-bully siya 3) Inaapi siya (?). Lahat mga bagay
    na normal lang naman na mga pangyayari sa kahit sino mang katutubo o hindi katutubo na Pilipino.
